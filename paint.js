addEventListener("load", init);

function init() {
	let context = {started:false, canvas:null, context:null, stampId:"", lastColor:"black", enableDraw:false};
	const colors = ["red", "pink", "fuchsia", "orange", "yellow", "lime", "green", "blue", 
		"purple", "black", "white"];
	const stamps = ["cat", "dog", "ladybug", "heart"];

	context.canvas = document.getElementById("imageView");
	context.context = context.canvas.getContext("2d");
	context.canvas.width  = 1024;
	context.canvas.height = 768;
	context.canvas.addEventListener("mousemove", e => onMouseMove(e, context));
	context.canvas.addEventListener("mouseout", e => endDraw(e, context));
	context.canvas.addEventListener("mousedown", e => context.enableDraw = true);
	context.canvas.addEventListener("mouseup", e => onMouseUp(e, context));
	for (let s of colors) {
		let colorDiv = document.createElement("div");
		colorDiv.id = s;
		colorDiv.style.cssText = "background:" + s + "; width:50px; height:47px;";
		document.getElementById("colorplace").appendChild(colorDiv);
	}
	document.getElementById("black").style.border = "2px dashed silver";
	for (let s of stamps) {
		let stampDiv = document.createElement("div");
		stampDiv.id = s;
		document.getElementById("stampplace").appendChild(stampDiv);
		let imgElement = document.createElement("img");
		imgElement.id = s + "Img";
		imgElement.src = s + ".png";
		imgElement.width = 50;
		imgElement.height = 50;
		stampDiv.appendChild(imgElement);
	}
	colors.forEach(s => document.getElementById(s).addEventListener("click", e => onColor(e, context)));
	stamps.forEach(s => document.getElementById(s).addEventListener("click", e => onStamp(e, context)));
	document.getElementById("fill").addEventListener("click", e => onFill(e, context));
	context.context.fillStyle = "white";
	context.context.lineWidth = "5";
	context.context.fillRect(0, 0, context.canvas.width, context.canvas.height);
}

function endDraw(e, context) {
	context.enableDraw = false;
	context.started = false;
	context.context.closePath();

}

function onMouseMove(e, context) {
	if (context.enableDraw && context.stampId.length < 1) {
		if (!context.started) {
			context.started = true;
			context.context.beginPath();
			context.context.moveTo(e.offsetX, e.offsetY);		
		} else
			context.context.lineTo(e.offsetX, e.offsetY);
		context.context.stroke();
	}
	document.getElementById("stats").innerHTML = e.offsetX + ", " + e.offsetY;
}

function onMouseUp(e, context) {
	endDraw(e, context);
	if (context.stampId.length > 0)
		context.context.drawImage(document.getElementById(context.stampId), e.pageX - 90, 
			e.pageY - 60, 80, 80);
}

function onColor(e, context) {
	context.context.strokeStyle = e.target.id;
	document.getElementById(context.lastColor).style.border = "0px dashed silver";
	document.getElementById(e.target.id).style.border = "2px dashed silver";
	context.lastColor = e.target.id;
	if (context.stampId.length > 0) document.getElementById(context.stampId).style.border = "0px dashed silver";
	context.stampId = "";
}

function onFill(e, context) {
	document.getElementById(context.lastColor).style.border = "0px dashed silver";
	context.context.fillStyle = context.context.strokeStyle;
	context.context.fillRect(0, 0, context.canvas.width, context.canvas.height);
	if (context.stampId.length > 0) document.getElementById(context.stampId).style.border = "0px dashed silver";
	context.stampId = "";
}

function onStamp(e, context) {
	document.getElementById(context.lastColor).style.border = "0px dashed silver";
	if (context.stampId.length > 0) document.getElementById(context.stampId).style.border = "0px dashed silver";
	context.stampId = e.target.id;
	document.getElementById(context.stampId).style.border = "2px dashed silver";
}

